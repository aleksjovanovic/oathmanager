#!/bin/bash

ABS_DIR=$(dirname `realpath -s $0`)

# check if package is installed
function isInstalled() {
    command -v $1 > /dev/null 2>&1
}

isInstalled oathmanager
if [ $? -ne 0 ]; then
    isInstalled pacman
    if [ $? -ne 0 ]; then
        echo "only pacman is supported yet :("
        echo "you can manually install the following packages:"
        for line in $(cat $ABS_DIR/dependencies/pacman.txt)
        do
            echo "    $line"
        done
        exit 1
    fi

    echo "Checking dependencies:"
    for package in $(cat $ABS_DIR/dependencies/pacman.txt)
    do
        pacman -T $package
        if [ $? -eq 0 ]; then
            echo "    '$package' is already installed"
        else
            echo "    installing $package..."
            sudo pacman -S --needed $package
        fi
    done

    echo ""
    echo "linking oathmanager..."
    sudo ln -sf $ABS_DIR/oathmanager.sh /usr/bin/oathmanager
    if [ $? -ne 0 ]; then
        echo "ERROR: Couldn't create symlink"
        exit 2
    fi

    echo ""
    echo "Restart services for current session..."
    systemctl is-active pcscd.service
    if [ $? -ne 0 ]; then
        sudo systemctl restart pcscd.service
    fi

    echo ""
    echo "Enable services for future use..."
    systemctl is-enabled pcscd.service
    if [ $? -ne 0 ]; then
        sudo systemctl enable pcscd.service
    fi

    echo "Syncing system time..."
    sudo ntpdate pool.npt.org
   
    echo "oathmanager successfully installed"
    exit 0
fi

echo "oathmanager is already installed"
