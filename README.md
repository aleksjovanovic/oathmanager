# Description 

oathmanager is an OATH integration for dmenu. It's based on the yubikey-manager and requires such a device. Check out https://www.yubico.com/ for more information.

# Prerequisites
1. compatible yubikey (you have to buy one)
2. yubikey-manager
3. dmenu
4. xclip
5. xdotool

# How does it work?
Todo...
Test it here: https://authenticator.ppl.family/

# Getting Started
Todo...

# Todo
- QR Code screen capture
    - Parsing of QR code check: https://github.com/google/google-authenticator/wiki/Key-Uri-Format
