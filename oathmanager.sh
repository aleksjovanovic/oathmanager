#!/bin/bash

# List all available secrets on the Yubikey
function selectCredentialFromYubikey() {
    LIST=$(ykman oath list)
    case "$?" in
        "0")
            ykman oath list | dmenu -l 10
            ;;
        "2")
            notify-send "Please insert Yubikey first."
            ;;
        *)
            notify-send "Unknown ERROR."
            ;;
    esac
}

# 1. Select oath from list
SELECTED_CREDENTIAL=$(selectCredentialFromYubikey)

if [ "$SELECTED_CREDENTIAL" != "" ]; then
    # 2. Generate code
    CODE=$(ykman oath code "$SELECTED_CREDENTIAL")

    # 3. Write out code
    #xdotool type $CODE
    notify-send "$CODE"
fi
