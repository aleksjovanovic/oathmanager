#!/bin/bash

count=$(ykman list | wc -l)
if [ $count -eq 0 ]; then
    echo "ERROR: No yubikey found!"
    exit 1
elif [ $count -gt 1 ]; then
    echo "ERROR: More than one yubikey found!"
    exit 2
fi

echo "how would you like to add the secret?"
echo "  s: screencapture (you select the QR code via mouse)"
echo "  m: manual (you copy/paste the secret in the terminal)"

mode=""
while [[ "$mode" != "s" ]] && [[ "$mode" != "S" ]] && [[ "$mode" != "m" ]] && [[ "$mode" != "M" ]]
do
    echo "what's gonna be? (s/m)?"
    read mode
done

case $mode in
    [mM])
        ykman oath add $alias
        ;;
    [sS])
        echo "Please drag a rectangle around the given QR Code"
        import /tmp/oathmanager_qrcode.png
        qr=$(zbarimg /tmp/oathmanager_qrcode.png)
        
        while [ $? -ne 0 ]
        do
            echo "ERROR: Something wen't wrong, please try again"
            import /tmp/oathmanager_qrcode.png
            qr=$(zbarimg /tmp/oathmanager_qrcode.png)
        done
            
        uri=$(echo $qr | sed -e "s/QR-Code://g")
        ykman oath uri "$uri"
        ;;
esac

#ykman oath add $alias
